require './tile_bag.rb'
require './tile_group.rb'

# Class that inherits from TileGroup to represent a group of tiles
# that make a word
class Word < TileGroup

	# Initializes the word as a new TileGroup
	# def initialize
		# @word = TileGroup.new
	# end
	
	# Creates a getter for the word
	# attr_reader :word
	
	# Gets the score for the whole word
	# @return [Integer] score for all tiles in the word
	def score
		score = 0
		self.tiles.each { |tile| score += TileBag.points_for(tile) } 
		return score
	end

end