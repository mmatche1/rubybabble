require './tile_bag.rb'
require './tile_rack.rb'
require './word.rb'
require 'spellchecker'
require 'tempfile'

# Class that runs the game of Babble
class Babble
	
	# Initializes all necessary objects for the Babble game
	def initialize
		@bag = TileBag.new
		@rack = TileRack.new
		@word = Word.new
	end
	
	# Runs the Babble game
	def run
		userCommand = nil
		userScore = 0
		until @bag.empty? do
			wordScore = 0
			@rack.number_of_tiles_needed.times do
				@rack.append(@bag.draw_tile)
			end
			puts "Current Tile Rack: " + @rack.hand
			puts "Enter a word that consists of letters in your rack"
			command = gets
			userCommand = command.chomp
			if userCommand == ':quit'
				break
			end
			if @rack.has_tiles_for?(userCommand)
				if Spellchecker::check(userCommand)[0][:correct]
					@word = @rack.remove_word(userCommand)
					wordScore = @word.score
					puts "You made " + @word.hand + " for " + wordScore.to_s + " points"
				else
					puts "Not a valid word"
				end
			else
				puts "Not enough tiles"
			end
			userScore += wordScore
			puts "Total points: " + userScore.to_s
		end
		puts "Thanks for playing!  Total score: " + userScore.to_s
	end

end

Babble.new.run