require 'minitest/autorun'
require_relative '../../word.rb'

# Test class for the score method in the Word class
class TestScore < Minitest::Test
	
	# Initializes the word for testing
	def setup
		@word = Word.new
	end
	
	# Ensures that an empty word returns a score of 0
	def test_empty_word_should_have_score_of_zero
		assert_equal 0, @word.score
	end
	
	# Ensures that a single letter word returns the correct score
	# A is word 1 point
	def test_score_a_one_tile_word
		@word.append :A
		assert_equal 1, @word.score
	end
	
	# Ensures that a multi letter word returns the correct score
	# A is worth 1 points
	# Z is worth 10 points
	def test_score_a_word_with_multiple_different_tiles
		@word.append :A
		@word.append :Z
		
		assert_equal 11, @word.score
	end
	
	# Ensures that a word with recurring letters returns 
	# the correct score
	def test_score_a_word_with_recurring_tiles
		@word.append :A
		@word.append :A
		@word.append :A
		
		assert_equal 3, @word.score
	end
	
end