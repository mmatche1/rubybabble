require 'minitest/autorun'
require_relative '../../word.rb'

# Tests the initialize method of the Word class
class TestInitialize < Minitest::Test
	
	# Initializes the word for testing
	def setup
		@word = Word.new
	end
	
	# Tests to ensure that an empty word is created at the start
	def test_create_empty_word
		assert_equal 0, @word.tiles.count		
	end 
	
end