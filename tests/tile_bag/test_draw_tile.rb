require 'minitest/autorun'
require_relative '../../tile_bag.rb'

# Test class for draw_tile method in the TileBag class
class TestDrawTile < Minitest::Test
	
	# Initializes the TileBag object for use in testing
	def setup
		@tileBag = TileBag.new
	end
	
	# Test to make sure there are 98 tiles at start of game
	def test_has_proper_number_of_tiles
		98.times { @tileBag.draw_tile }

		assert_equal true, @tileBag.empty?
		
	end
	
	# Test to check for correct distribution of tiles
	def test_has_proper_tile_distribution
		tileCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
					0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
					0, 0, 0, 0, 0, 0, 0]
		98.times do	
			tile = @tileBag.draw_tile
			if tile == :A
				tileCount[0] += 1
			elsif tile == :B
				tileCount[1] += 1
			elsif tile == :C
				tileCount[2] += 1
			elsif tile == :D
				tileCount[3] += 1
			elsif tile == :E
				tileCount[4] += 1
			elsif tile == :F
				tileCount[5] += 1
			elsif tile == :G
				tileCount[6] += 1
			elsif tile == :H
				tileCount[7] += 1
			elsif tile == :I
				tileCount[8] += 1
			elsif tile == :J
				tileCount[9] += 1
			elsif tile == :K
				tileCount[10] += 1
			elsif tile == :L
				tileCount[11] += 1
			elsif tile == :M
				tileCount[12] += 1
			elsif tile == :N
				tileCount[13] += 1
			elsif tile == :O
				tileCount[14] += 1
			elsif tile == :P
				tileCount[15] += 1
			elsif tile == :Q
				tileCount[16] += 1
			elsif tile == :R
				tileCount[17] += 1
			elsif tile == :S
				tileCount[18] += 1
			elsif tile == :T
				tileCount[19] += 1
			elsif tile == :U
				tileCount[20] += 1
			elsif tile == :V
				tileCount[21] += 1
			elsif tile == :W
				tileCount[22] += 1
			elsif tile == :X
				tileCount[23] += 1
			elsif tile == :Y
				tileCount[24] += 1
			elsif tile == :Z
				tileCount[25] += 1
			else 
				tileCount[26] += 1
			end
		end
		assert_equal [9, 2, 2, 4, 12, 2, 3, 2, 9, 1, 
						1, 4, 2, 6, 8, 2, 1, 6, 4, 6, 
						4, 2, 2, 1, 2, 1, 0], tileCount		
	end
	
end