require 'minitest/autorun'
require_relative '../../tile_bag.rb'

# Test class for the points_for method of the TileBag class
class TestPointsFor < Minitest::Test

	# Sets up the tileBag object and all arrays used in testing
	def setup
		tileBag = TileBag.new
		@onePtTiles = [:A, :E, :I, :O, :N, :R, :T, :L, :S, :U]
		@twoPtTiles = [:D, :G]
		@threePtTiles = [:B, :C, :M, :P]
		@fourPtTiles = [:F, :H, :V, :W, :Y]
		@fivePtTiles = [:K]
		@eightPtTiles = [:J, :X]
		@tenPtTiles = [:Q, :Z]
	end
	
	# Tests to confirm that all point values are set correctly
	def test_confirm_point_values 
		correct = true
		@onePtTiles.each do |onePtTile|
			if TileBag.points_for(onePtTile) != 1
				correct = false
			end
		end
		@twoPtTiles.each do |twoPtTile|
			if TileBag.points_for(twoPtTile) != 2
				correct = false
			end
		end
		@threePtTiles.each do |threePtTile|
			if TileBag.points_for(threePtTile) != 3
				correct = false
			end
		end
		@fourPtTiles.each do |fourPtTile|
			if TileBag.points_for(fourPtTile) != 4
				correct = false
			end
		end
		@fivePtTiles.each do |fivePtTile|
			if TileBag.points_for(fivePtTile) != 5
				correct = false
			end
		end
		@eightPtTiles.each do |eightPtTile|
			if TileBag.points_for(eightPtTile) != 8
				correct = false
			end
		end
		@tenPtTiles.each do |tenPtTile|
			if TileBag.points_for(tenPtTile) != 10
				correct = false
			end
		end
		
		assert_equal true, correct
		
	end	
end