require 'minitest/autorun'
require_relative '../../tile_rack.rb'

# Test class for the number_of_tiles_needed method of the 
# TileRack class
class TestNumberOfTilesNeeded < Minitest::Test
	
	# Initializes the TileGroup for testing
	def setup
		@tileRack = TileRack.new
	end
	
	# Confirms that an empty tile rack should require 7 tiles
	def test_empty_tile_rack_should_need_max_tiles
		assert_equal 7, @tileRack.number_of_tiles_needed
	end
	
	# Confirms that a rack with one tile should require 6 tiles
	def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles
		@tileRack.append :A
		assert_equal 6, @tileRack.number_of_tiles_needed
	end
	
	# Confirms that a rack with some tiles should require
	# 7 - tiles.count tiles
	def test_tile_rack_with_several_tiles_should_need_some_tiles
		@tileRack.append :A
		@tileRack.append :B
		@tileRack.append :C
		assert_equal 4, @tileRack.number_of_tiles_needed
	end
	
	# Confirms that a full rack should not require any more tiles
	def test_that_full_tile_rack_doesnt_need_any_tiles
		@tileRack.append :A
		@tileRack.append :B
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :B
		@tileRack.append :C
		@tileRack.append :Z
		assert_equal 0, @tileRack.number_of_tiles_needed
	end
	
end