require 'minitest/autorun'
require_relative '../../tile_rack.rb'

# Test class for the remove word method of the TileRack class
class TestRemoveWord < Minitest::Test
	
	# Initializes the TileRack for testing
	def setup
		@tileRack = TileRack.new
	end
	
	# Confirms that letters in order can be removed from the rack
	def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :T
		@tileRack.append :Z
		@tileRack.remove_word('cat')
		
		# assert_equal "CAT", @tileRack.remove_word("cat").hand
		assert_equal [:Z], @tileRack.tiles
	end
	
	# Confirms that letters out of order can be removed from the rack
	def test_can_remove_a_word_whose_letters_are_not_in_order_on_the_rack
		@tileRack.append :C
		@tileRack.append :T
		@tileRack.append :Z
		@tileRack.append :A
		@tileRack.remove_word('cat')
		
		# assert_equal "CAT", @tileRack.remove_word("cat").hand
		assert_equal [:Z], @tileRack.tiles
	end
	
	# Confirms that words with duplicate letters can be removed 
	# from the rack
	def test_can_remove_word_with_duplicate_letters
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :C
		@tileRack.append :T
		@tileRack.append :U
		@tileRack.append :S
		@tileRack.append :Z
		@tileRack.remove_word('cactus')
		
		# assert_equal "CAT", @tileRack.remove_word("cat").hand
		assert_equal [:Z], @tileRack.tiles
	end
	
	# Confirms that words can be removed without removed duplicates
	# that are not needed for the word.
	def test_can_remove_word_without_removing_unneeded_duplicate_letters
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :C
		@tileRack.append :T
		@tileRack.append :U
		@tileRack.append :S
		@tileRack.remove_word('cat')
		
		assert_equal [:C, :U, :S], @tileRack.tiles
	end
	
end