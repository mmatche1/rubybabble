require 'minitest/autorun'
require_relative '../../tile_rack.rb'

# Test class for the has_tiles_for? method of the TileRack class
class TestHasTilesFor < Minitest::Test
	
	# Initializes the TileRack for testing
	def setup
		@tileRack = TileRack.new
	end
	
	# Confirms that the rack can match a word whose letters 
	# are in the same order on the rack as in the given word
	def test_rack_has_needed_letters_when_letters_are_in_order_no_duplicates
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :T
		assert_equal true, @tileRack.has_tiles_for?('cat')
	end
	
	# Confirms that the rack contains the text given even
	# if rack has tiles out of order
	def test_rack_has_needed_letters_when_letters_are_not_in_order_no_duplicates
		@tileRack.append :T
		@tileRack.append :C
		@tileRack.append :A
		assert_equal true, @tileRack.has_tiles_for?('cat')
	end
	
	# Confirms that false is returned when no letters
	# from the given text match the rack
	def test_rack_doesnt_contain_any_needed_letters
		assert_equal false, @tileRack.has_tiles_for?('cat')
	end
	
	# Confirms that false is returned when some but not all
	# letters match the given text
	def test_rack_contains_some_but_not_all_needed_letters
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :R
		assert_equal false, @tileRack.has_tiles_for?('cat')
	end
	
	# Confirms that true is returned when given text 
	# containing duplicates is contained in rack
	def test_rack_contains_a_word_with_duplicate_letters
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :C
		@tileRack.append :T
		@tileRack.append :U
		@tileRack.append :S
		assert_equal true, @tileRack.has_tiles_for?('cactus')
	end
	
	# Confirms false is returned when not enough duplicates
	# are in the rack to match given text
	def test_rack_doesnt_contain_enough_duplicate_letters
		@tileRack.append :C
		@tileRack.append :A
		@tileRack.append :U
		@tileRack.append :T
		@tileRack.append :S
		assert_equal false, @tileRack.has_tiles_for?('cactus')
	end
	
end