require 'minitest/autorun'
require_relative '../../tile_group.rb'

# Test class for the hand method of the TileGroup class
class TestHand < Minitest::Test
	
	# Initializes the TileGroup for testing
	def setup
		@tileGroup = TileGroup.new
	end
	
	# Tests that an empty group returns the correct string
	def test_convert_empty_group_to_string
		hand = @tileGroup.hand
		assert_equal '', hand
	end
	
	# Tests that a single tile group returns the correct string
	def test_convert_single_tile_group_to_string
		@tileGroup.append :A
		hand = @tileGroup.hand
		assert_equal 'A', hand
	end
	
	# Tests that a multi tile group returns the correct string
	def test_convert_multi_tile_group_to_string
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :C
		hand = @tileGroup.hand
		assert_equal 'ABC', hand
	end
	
	# Tests that a multi tile group (with duplicates)returns
	# the correct string
	def test_convert_multi_tile_group_with_duplicates_to_string
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :B
		@tileGroup.append :C
		hand = @tileGroup.hand
		assert_equal 'ABBC', hand
	end
end