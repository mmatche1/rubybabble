require 'minitest/autorun'
require_relative '../../tile_group.rb'

# Test class for the append method of the TileGroup class
class TestRemove < Minitest::Test
	
	# Initializes the TileGroup object for testing
	def setup
		@tileGroup = TileGroup.new
	end
	
	# Tests that the only tile in the group can be removed
	def test_remove_only_tile
		@tileGroup.append :A
		@tileGroup.remove :A
		
		assert_equal 0, @tileGroup.tiles.count
	end
	
	# Tests that the first tile out of many can be removed 
	# from the group
	def test_remove_first_tile_from_many
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :C
		
		@tileGroup.remove :A
		
		assert_equal [:B, :C], @tileGroup.tiles
	end
	
	# Tests that the last tile out of many can be removed 
	# from the group
	def test_remove_last_file_from_many
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :C
		
		@tileGroup.remove :C
		
		assert_equal [:A, :B], @tileGroup.tiles
	end
	
	# Tests that the middle tile out of many can be removed 
	# from the group
	def test_remove_middle_tile_from_many
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :C
		
		@tileGroup.remove :B
		
		assert_equal [:A, :C], @tileGroup.tiles
	end
	
	# Tests that the multiple tiles can be removed from the group
	def test_remove_multiple_tiles
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :C
		
		@tileGroup.remove :A
		@tileGroup.remove :C
		
		assert_equal [:B], @tileGroup.tiles
	end
	
	# Tests that one of multiple duplicate tiles can be removed 
	# from the group
	def test_make_sure_duplicates_are_not_removed
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :B
		@tileGroup.append :C
		
		@tileGroup.remove :B
		
		assert_equal [:A, :B, :C], @tileGroup.tiles
	end
end