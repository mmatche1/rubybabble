require 'minitest/autorun'
require_relative '../../tile_group.rb'

# Test class for the append method in the TileGroup class
class TestAppend < Minitest::Test
	
	# Initializes the TileGroup for testing
	def setup
		@tileGroup = TileGroup.new
	end
	
	# Confirms that a single tile can be appended to the group
	def test_append_one_tile
		@tileGroup.append :A
		assert_equal [:A], @tileGroup.tiles
	end
	
	# Confirms that many tiles can be appended to the group
	def test_append_many_tiles
		@tileGroup.append :A
		@tileGroup.append :B
		@tileGroup.append :C
		assert_equal [:A, :B, :C], @tileGroup.tiles
	end
	
	# Confirms that duplicate tiles can be appended to the group
	def test_append_duplicate_tiles
		@tileGroup.append :C
		@tileGroup.append :C
		assert_equal [:C, :C], @tileGroup.tiles
	end
	
end