require 'minitest/autorun'
require_relative '../../tile_group.rb'

#Test class for the initialize method of the TileGroup class
class TestInitialize < Minitest::Test
	
	# Sets up the TileGroup object for testing
	def setup
		@tiles = TileGroup.new
	end
	
	# Tests that a TileGroup starts with an empty set of tiles
	#
	# For some reason this test ONLY will not recognize the setup method
	# and insists that @tileGroup is nil unless I initialize it in this
	# test!
	def test_create_empty_tile_group
		@tileGroup = TileGroup.new
		assert_equal 0, @tileGroup.tiles.count
	end
	
end