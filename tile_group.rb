# Class that holds a group of specific tiles
class TileGroup

	# Initializes the group as an empty array at start
	def initialize
		@tiles = []
	end
	
	# Creates getter and setter methods for the tiles array
	attr_accessor :tiles
	
	# Appends a given tile to the tiles array
	# @param [tile] tile to append
	def append(tile)
		@tiles << tile
	end
	
	# Removes a given tile from the tiles array
	# @param [tile] tile to remove
	def remove(tile)
		@tiles.delete_at(@tiles.index(tile) || @tiles.length)
	end
	
	# Converts the tiles array to a string
	# @return [String] string representation of the tile group
	def hand
		hand = ''
		tiles.each {|tile| hand = hand + tile.to_s}
		return hand
	end
	
end