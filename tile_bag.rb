# Holds all tiles available in the game and their point values
class TileBag 
	
	# Initialize the tile bag with a set of Scrabble tiles
	# @tileBag
	def initialize
		@tileBag = [ :A, :A, :A, :A, :A, :A, :A, :A, :A,
					:B, :B,
					:C, :C,
					:D, :D, :D, :D,
					:E, :E, :E, :E, :E, :E, :E, :E, :E, :E, :E, :E,
					:F, :F,
					:G, :G, :G,
					:H, :H,
					:I, :I, :I, :I, :I, :I, :I, :I, :I, 
					:J, :K, 
					:L, :L, :L, :L,
					:M, :M,
					:N, :N, :N, :N, :N, :N, 
					:O, :O, :O, :O, :O, :O, :O, :O, 
					:P, :P, :Q,
					:R, :R, :R, :R, :R, :R, 
					:S, :S, :S, :S, 
					:T, :T, :T, :T, :T, :T, 
					:U, :U, :U, :U, 
					:V, :V,
					:W, :W,
					:X, :Y, :Y, :Z ]
	end
	
	# Draws a random tile from the tile bag based on a random index
	# @return [tile] tile drawn
	def draw_tile
		randomTileIndex = rand(@tileBag.count - 1)
		@tileBag.delete_at(randomTileIndex)
	end
	
	# Checks whether the tile bag is empty or not
	# @return [true] if 0 tiles left in bag
	def empty?
		@tileBag.count == 0
	end
	
	# Finds the points for a given tile
	# @param [tile] tile to get points for
	# @return [int] points for tile
	def self.points_for(tile)
		tilePoints = {
			:A => 1,
			:B => 3,
			:C => 3,
			:D => 2,
			:E => 1,
			:F => 4,
			:G => 2,
			:H => 4,
			:I => 1,
			:J => 8,
			:K => 5,
			:L => 1,
			:M => 3,
			:N => 1,
			:O => 1,
			:P => 3,
			:Q => 10,
			:R => 1,
			:S => 1,
			:T => 1,
			:U => 1,
			:V => 4,
			:W => 4,
			:X => 8,
			:Y => 4,
			:Z => 10 }
			
			return tilePoints[tile]
	end
	
end			