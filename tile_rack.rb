require './tile_group.rb'

# Class to hold the rack of 7 tiles available for use
class TileRack < TileGroup

	# attr_reader :tileRack
	
	# Initialize the TileGroup object that will hold this 
	# set of tiles
	# def initialize
		# @tileRack = TileGroup.new
	# end
	
	# Checks how many tiles are needed to refresh the rack
	# @return Integer number of tiles needed
	def number_of_tiles_needed
		tilesNeeded = 7 - self.tiles.count
		return tilesNeeded
	end
	
	# Checks whether the rack has the tiles for the word
	# @return Boolean true if tiles available
	# @param String word to check
	def has_tiles_for?(text)
		text.upcase!
		# return @tileRack.hand.eql? text
		text.chars.uniq.all? {|tile| text.count(tile) <= self.hand.count(tile)}
	end
	
	# Removes the tiles of the given word from the rack
	# @param String word to remove
	# @return Word object created from tiles removed
	def remove_word(text)	
		newWord = Word.new
		#if (has_tiles_for?(text))
			text.upcase!
			text.each_char do |tile|
				# newWord.tiles.push(self.remove(tile))
				# tileIndex = self.tiles.index(tile)
				newWord.append(tile.intern)
				remove(tile.intern)
			end
		#end
		return newWord
	end
end